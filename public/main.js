$(document).ready(function() {

      var myarray = [];
      var landingPageRef;
      var landingPageGeneralID;

      var gui = require('nw.gui');
      var win = gui.Window.get();

      var Eloqua = require('eloqua-request');
      var eloqua = new Eloqua('PhilipsConsumerLifestyle', 'Mukerrem.Akkoyun', 'Mukerrem.A$_09');


      function createName(_landingPageId, _callback) {
            var date = new Date();
            var full_date = date.toDateString() + "  " + date.toLocaleTimeString() + " " + date.getUTCMilliseconds();
            full_date = full_date.replace(/:/g, "_");
            var file_name = _landingPageId + "  " + full_date + ".txt";
            _callback(file_name);
      }


      document.addEventListener('DOMContentLoaded', function() {
            if (Notification.permission !== "granted") {
                  Notification.requestPermission();
            }
      });


      function notifyMe(message, status, title) {

            if (!Notification) {
                  alert('Desktop notifications not available in your browser. Try Chromium.');
                  return;
            }

            if (Notification.permission !== "granted") {
                  Notification.requestPermission();
            } else {
                  var icon = "";
                  var _title = "";

                  if (status == "success") {
                        icon = 'public/tick_green.png';
                        _title = "Success";
                  } else {
                        icon = 'public/warning_exclamation.png';
                        _title = "Error";
                  }

                  var notification = new Notification(_title, {
                        icon: icon,
                        body: message,
                  });

                  notification.onclick = function() {
                        gui.Shell.openItem("C:\\Users\\serkan.demirel\\Desktop\\backups\\");
                  };

            }

      }



      function backup(_fileName, _value, _callback) {

            var fs = require('fs');

            fs.writeFile("C:\\Users\\serkan.demirel\\Desktop\\backups\\" + _fileName, _value, function(err) {

                  if (err) {
                        alert("File couldn't back up!!!");
                        return false;
                  }

                  _callback();

            });
      }

      function resetPage() {
            $("#original").val("");
            $("#new_version").val("");
            $("#get").prop("disabled", false);
            $("#landingPageId").val("");
            landingPageRef = null;
            landingPageGeneralID = null;
      }



      function elequaGet(_landingPageId, _callback) {

            var urlGET = '/API/REST/2.0/assets/landingPage/' + _landingPageId + '?depth=complete&extensions=e10';
            eloqua.get(urlGET, function(err, response) {

                  landingPageGeneralID = _landingPageId;

                  if (err) {
                        notifyMe("Landing page couldn't find!", "error", "Error");
                        resetPage();
                        return false;
                  }

                  if (typeof _callback === "function") {
                        _callback(response);
                  }



            });

      }

      function elequaUpdate(_landingPageId, _data, _callback) {

            var urlPUT = '/API/REST/2.0/assets/landingPage/' + _landingPageId + '?extensions=e10';
            eloqua.put(urlPUT, _data, function(err, response) {
                  if (err) {
                        notifyMe("Landing page couldn't find!", "error", "Error");
                        resetPage();
                        return false;
                  }

                  if (typeof _callback === "function") {
                        _callback(response);
                  }



            });

      }




      $("#get").click(function() {

            $("#original").val("");
            $("#new_version").val("");
            landingPageRef = null;

            var landingPageId = $("#landingPageId").val();

            //check if id is number!!
            if (landingPageId) {

                  $("#get").prop("disabled", true);
                  $("#update").prop("disabled", true);

                  //get data
                  elequaGet(landingPageId, function(_response) {

                        landingPageRef = _response;

                        var rawHtmlContent = _response.htmlContent.html;

                        $("#original").val(rawHtmlContent);
                        $("#original").attr("disabled", "disabled");

                        $("#new_version").val(rawHtmlContent);

                        //name create
                        createName(landingPageId, function(_name) {

                              //backup
                              backup(_name, rawHtmlContent, function() {
                                    $("#get").prop("disabled", false);
                                    $("#update").prop("disabled", false);
                                    notifyMe("Landing page has received and backed up!", "success", "Success");
                              });

                        });
                  });


            } else {
                  alert("Please spesify the Landing Page ID.");
            }




      });


      $("#update").click(function() {


            var lpTextBoxID = $("#landingPageId").val();

            if (landingPageRef && landingPageRef.id == lpTextBoxID) {

                  $("body *").prop("disabled", true);
                  var updateData = landingPageRef;
                  updateData.htmlContent.html = $("#new_version").val();

                  elequaUpdate(landingPageGeneralID, updateData, function(response) {

                        $("body *").prop("disabled", false);
                        resetPage();

                        $("#original").val(response.htmlContent.html);

                        notifyMe("Landing page has been updated!", "success", "Success");
                  });

            } else {
                  resetPage();
                  alert("Please spesify landing page ID and receive the page first!");
            }



      });





      $("#makeChanges").click(function() {
            //here is the regex code
            //htm.replace(/<\/head>/g,"aaaaaam </head>")
            var htmlContent = $("#new_version").val().trim()
            if(htmlContent == "")
            {
                        alert("Html content is empty!");
                        return false;
            }

            if(htmlContent.match(/<\/head>/g) == null )
            {
                  alert("There is no Head tag!");
                  return false;


            }

            if( htmlContent.match(/<\/head>/g).length != 1 ){
                  alert("There are more than one Head tag!");
                  return false;
            }

            if( htmlContent.match(/www\.crsc\.philips\.com\/analytics\/production\/satellitelib\.js/g) != null ){
                  alert("There is already a www.crsc.philips.com/analytics/production/satellitelib.js file exist!");
                  return false;
            }

            htmlContent = htmlContent.replace(/<\/head>/g,'<!-- cookie consent --> \n <script  src="//www.crsc.philips.com/analytics/production/satellitelib.js"></script> \n <!-- /cookie consent --> \n </head>');

            $("#new_version").val(htmlContent)



      });


      $("#backup").click(function() {

      });





});
